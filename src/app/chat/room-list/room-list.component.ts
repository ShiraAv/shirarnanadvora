import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {
messageList: FirebaseListObservable<any[]>;

  constructor(private af: AngularFire) { 
    this.messageList = af.database.list('rooms');
  }

  ngOnInit() {
  }

}
