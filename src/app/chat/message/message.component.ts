import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { AF } from "../../providers/af";
import { LoginPageComponent } from '../../login-page/login-page.component';
//import { RegistrationPageComponent } from '../../registration-page/registration-page.component';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  items: FirebaseListObservable<any[]>;

  constructor(private af: AngularFire) {
    this.items = af.database.list('rooms');
  }

  ngOnInit() {
  }

   sendMessage(inputElementSelf) {
     this.items.push({
       roomName: String(inputElementSelf.value)
     });
   }

}
