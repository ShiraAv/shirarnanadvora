import { ShiraRenanaDvoraPage } from './app.po';

describe('shira-renana-dvora App', () => {
  let page: ShiraRenanaDvoraPage;

  beforeEach(() => {
    page = new ShiraRenanaDvoraPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
